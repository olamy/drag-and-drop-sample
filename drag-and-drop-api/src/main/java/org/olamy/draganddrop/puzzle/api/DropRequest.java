package org.olamy.draganddrop.puzzle.api;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Olivier Lamy
 */
@XmlRootElement( name = "dropRequest" )
public class DropRequest
    implements Serializable
{
    /**
     * id of the received element
     */
    private String id;

    /**
     * src attribute value of the element received
     */
    private String src;

    public DropRequest()
    {
        // no op
    }

    public DropRequest( String id, String src )
    {
        this.id = id;
        this.src = src;
    }

    public String getId()
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getSrc()
    {
        return src;
    }

    public void setSrc( String src )
    {
        this.src = src;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append( "DropRequest" );
        sb.append( "{id='" ).append( id ).append( '\'' );
        sb.append( ", src='" ).append( src ).append( '\'' );
        sb.append( '}' );
        return sb.toString();
    }
}
