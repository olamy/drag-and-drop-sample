package org.olamy.draganddrop.puzzle.api;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Olivier Lamy
 */
@XmlRootElement( name = "dropResponse" )
public class DropResponse
    implements Serializable
{
    /**
     * status message from the server
     */
    private String statusMessage;

    private DropRequest dropRequest;

    /**
     * received date on the server
     * format see {@link DragAndDropService#RECEIVED_DATE_FORMAT}
     */
    private String receivedDate;

    public DropResponse()
    {
        // no op
    }

    public DropResponse( String statusMessage, DropRequest dropRequest, String receivedDate )
    {
        this.statusMessage = statusMessage;
        this.dropRequest = dropRequest;
        this.receivedDate = receivedDate;
    }

    public String getStatusMessage()
    {
        return statusMessage;
    }

    public void setStatusMessage( String statusMessage )
    {
        this.statusMessage = statusMessage;
    }

    public DropRequest getDropRequest()
    {
        return dropRequest;
    }

    public void setDropRequest( DropRequest dropRequest )
    {
        this.dropRequest = dropRequest;
    }

    public String getReceivedDate()
    {
        return receivedDate;
    }

    public void setReceivedDate( String receivedDate )
    {
        this.receivedDate = receivedDate;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append( "DropResponse" );
        sb.append( "{statusMessage='" ).append( statusMessage ).append( '\'' );
        sb.append( ", dropRequest=" ).append( dropRequest );
        sb.append( ", receivedDate='" ).append( receivedDate ).append( '\'' );
        sb.append( '}' );
        return sb.toString();
    }
}
