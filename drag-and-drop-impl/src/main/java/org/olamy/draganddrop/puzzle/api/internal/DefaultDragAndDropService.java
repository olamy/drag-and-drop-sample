package org.olamy.draganddrop.puzzle.api.internal;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.draganddrop.puzzle.api.DragAndDropService;
import org.olamy.draganddrop.puzzle.api.DropRequest;
import org.olamy.draganddrop.puzzle.api.DropResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Olivier Lamy
 */
@Service( "dragAndDropService#default" )
public class DefaultDragAndDropService
    implements DragAndDropService
{

    private Logger logger = LoggerFactory.getLogger( getClass() );

    public DropResponse receiveDrop( DropRequest dropRequest )
    {

        return new DropResponse( "OK well done", dropRequest,
                                 new SimpleDateFormat( RECEIVED_DATE_FORMAT ).format( new Date() ) );
    }
}
