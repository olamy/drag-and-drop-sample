Drag and drog javascript puzzle
-----

You need java 1.6 and maven 3.0.x to run the project.

It's possible to test the web application using:
* mvn tomcat7:run (hit your browser at http://localhost:9090/)
* using the embeded Apache Tomcat packaging. Build it using: mvn clean install.
  Then cd drag-and-drop-webapp-exec/target/ && java -jar drag-and-drop-webapp-exec-1.0-SNAPSHOT-war-exec.jar
  hit your browser at http://localhost:9090/


You will probably need to configure your settings.xml to be able to use tomcat7-maven-plugin.

Edit your ~/.m2/settings.xml

  <pluginGroups>
    <pluginGroup>org.apache.tomcat.maven</pluginGroup>
  </pluginGroups>


Application tested osx 10.7.4 with Firefox 12.0, Chrome 19.0.1084.54 and Safari 5.1.7.

You can run selenium test on Firefox default and chrome using -Pchrome.
