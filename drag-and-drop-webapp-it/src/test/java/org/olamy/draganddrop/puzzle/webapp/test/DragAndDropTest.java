package org.olamy.draganddrop.puzzle.webapp.test;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import com.thoughtworks.selenium.DefaultSelenium;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;

/**
 * @author Olivier Lamy
 */
public class DragAndDropTest
    extends TestCase
{

    public void testDragAndDrop()
        throws Exception
    {

        int seleniumPort = Integer.parseInt( System.getProperty( "selenium.port", "4444" ) );
        String browser = System.getProperty( "seleniumBrowser", "*firefox" );
        String serverUrl = System.getProperty( "serverUrl", "http://localhost:9090/" );

        long waitTime = Long.getLong( "selenium.waitTime" );

        DefaultSelenium s = new DefaultSelenium( "localhost", seleniumPort, browser, serverUrl );

        s.start();
        s.open( "index.html" );

        assertTrue( StringUtils.isBlank( s.getText( "//div[@id='webconsole']" ) ) );

        assertTrue( s.isVisible( "//ul[@id='draggable_source']//img[@id='img1']" ) );

        s.dragAndDropToObject( "//ul[@id='draggable_source']//img[@id='img1']", "//ul[@id='draggable_target_items']" );

        Thread.sleep( waitTime );

        String logText = s.getText( "//div[@id='webconsole']" );

        assertTrue( logText.contains( "Server status: OK well done" ) );
        assertTrue( logText.contains( "Request completed." ) );

        assertFalse( s.isVisible( "//ul[@id='draggable_source']//img[@id='img1']" ) );
        assertTrue( s.isVisible( "//ul[@id='draggable_target_items']//img[@id='img1']" ) );


    }

}
