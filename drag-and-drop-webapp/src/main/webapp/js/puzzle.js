/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

$( document ).ready( function()
{
  consoleLog = function(message) {
    if (typeof window.console != 'undefined' && typeof window.console.log != 'undefined') {
      console.log(message);
    } else {
      // do nothing no console
    }
  };

  webConsoleLog=function(level,message){
    $("#webconsole" ).append(level + ':' + message + "<br/>");
  }

  webConsoleLogInfo=function(message){
    webConsoleLog('[INFO]',message);
  }

  webConsoleLogError=function(message){
    webConsoleLog('[ERROR]',message);
  }

  var draggableSource = $( '#draggable_source');

  $( '#draggable_source img' ).draggable({
    //containment: "#draggable_target"
    //revert: true
  });

  var draggableTarget = $("#draggable_target" );

  draggableTarget.droppable({
      drop: function(event, ui) {
        var sourceElement = $(ui.helper );
        var imgId=sourceElement.attr("id");
        var altValue=sourceElement.attr("alt");
        var srcValue=sourceElement.attr("src");
        consoleLog("draggable_target#drop:"+imgId);
        // remove element from source
        draggableSource.find("#"+imgId ).parent().hide();
        // add it in target
        // <li class="span3"><img alt="first" id="img1" /></li>
        draggableTarget.find("#draggable_target_items" )
            .append('<li class="span2"><img alt="'+altValue+'" id="'+imgId+'" src="'+srcValue+'" /></li>');
        $.ajax({
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            url: '/services/restServices/DragAndDropService/drop',
            data:$.toJSON( {
              id: imgId,
              src: srcValue
            }),
            beforeSend: function(){
              webConsoleLogInfo( 'Starting request.' );
            },
            complete: function(){
              webConsoleLogInfo( 'Request completed.' );
            },
            success: function( data, textStatus, xhr ){
              webConsoleLogInfo( data.receivedDate + ' element with id: ' + imgId + ' moved. Server status: ' + data.statusMessage );
            },
            error: function( data, textStatus, xhr ){
              // TODO cancel element drag&drop
              webConsoleLogError( xhr.status+ ' : '+ xhr.statusText );
            }
        } );
    }
  });

});